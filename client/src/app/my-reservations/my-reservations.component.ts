import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs'; 
import { ActivatedRoute, Router } from '@angular/router';
import { UserService } from '../services/user.service';
import { UserModel } from '../models/user.model';
import { VillaModel } from '../models/villa.model';
import { HotelModel } from '../models/hotel.model';
import { ApartmentModel } from '../models/apartment.model';

@Component({
  selector: 'app-my-reservations',
  templateUrl: './my-reservations.component.html',
  styleUrls: ['./my-reservations.component.css']
})
export class MyReservationsComponent implements OnInit {
  
  public user: UserModel;
  
  constructor(
        private route: ActivatedRoute,
        private router: Router,
        private userService: UserService) { 
         this.userService.getUserById()
          .subscribe((user: UserModel) => {
            this.user = user;
            });        
  console.log(this.user);
  }
  
  ngOnInit(): void {
  }

}
