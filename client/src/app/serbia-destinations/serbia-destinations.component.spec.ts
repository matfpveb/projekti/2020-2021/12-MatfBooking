import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SerbiaDestinationsComponent } from './serbia-destinations.component';

describe('SerbiaDestinationsComponent', () => {
  let component: SerbiaDestinationsComponent;
  let fixture: ComponentFixture<SerbiaDestinationsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SerbiaDestinationsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SerbiaDestinationsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
