import { Component, OnInit } from '@angular/core';
import { MapService} from '../services/map.service';

@Component({
  selector: 'app-map',
  templateUrl: './map.component.html',
  styleUrls: ['./map.component.css']
})
export class MapComponent implements OnInit {
  
  latitude: number = 44.786568;
  longitude:number= 20.448921;
  zoom = 13;


  constructor( private mapService: MapService) { }

  ngOnInit(): void {
  
  }

  
  public onMapReady(){}
    

}
