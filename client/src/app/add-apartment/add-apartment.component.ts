import { ApartmentService } from './../services/apartment.service';
import { Router } from '@angular/router';
import { Apartment } from './../models/apartment.model';
import { FormGroup } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import { HttpErrorResponse, HttpClient } from '@angular/common/http';
import { UserService } from '../services/user.service';
import { UserModel } from '../models/user.model';
import { Observable } from 'rxjs';



@Component({
  selector: 'app-add-apartment',
  templateUrl: './add-apartment.component.html',
  styleUrls: ['./add-apartment.component.css']
})
export class AddApartmentComponent implements OnInit {

  newApartment: Apartment;
  errors: any[] = [];
  selectedFile: File =null;


  constructor(private httpClient:HttpClient,private apartmentService: ApartmentService, private userService: UserService, private router: Router,private http: HttpClient) { }

  ngOnInit(): void {
    this.newApartment = new Apartment();
  }

  public currentUser():UserModel{
    return this.userService.currentUser;
  }


postFile(fileToUpload: File): Observable<any> {
  const endpoint = 'http://localhost:3000/apartments/file'
  const formData: FormData = new FormData();
  formData.append('image', fileToUpload, fileToUpload.name);
  return this.httpClient
    .post(endpoint, formData);
}
handleImageUpload(files:FileList){
  this.selectedFile=files.item(0);
  this.postFile(this.selectedFile).subscribe((response) => {
    console.log('response received is ', response);
    this.newApartment.images="../../assets/img/gallery/" + this.selectedFile.name;
})
}
  public createApartment(){
   
    this.apartmentService.addNewApartment(this.newApartment).subscribe(
      (rental: Apartment)=>{
        alert('Apartment is successfully created!');
        this.router.navigate([`/`])
      },
      (errorresponse: HttpErrorResponse)=>{
          this.errors = errorresponse.error.errors;
      }
    )
  }



}
