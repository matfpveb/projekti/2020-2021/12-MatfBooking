import { Component, OnDestroy, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import { UserService } from '../services/user.service';
import { Router, ActivatedRoute} from '@angular/router';
import { Subscription } from 'rxjs';
import { UserModel } from '../models/user.model';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit, OnDestroy{

  public loginForm: FormGroup;
  public errors: any[] = [];
  public notifyMessage: string = '';

  private activeSubs: Subscription[]=[];
  public user: UserModel[];
  public userByEmail: UserModel;
  public currentEmail: string;

  constructor(private fb: FormBuilder,private userServ:UserService, private router: Router, private route: ActivatedRoute) { 
    this.loginForm = this.fb.group({
      email: ['', [Validators.required, Validators.pattern('^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$')]],
      password: ['', Validators.required]
    });

    this.user = userServ.getUsers();
  }

  ngOnInit(): void {


  }
  
  public isInvalidInput(fieldName){
    return this.loginForm.controls[fieldName].invalid && (this.loginForm.controls[fieldName].dirty || this.loginForm.controls[fieldName].touched)
  }
  public isRequired(fieldName){
    return this.loginForm.controls[fieldName].errors.required
  }

  public email(){
    return this.loginForm.get('email');
  }

  public password(){
    return this.loginForm.get('password');
  }

  public submitForm(data){
    console.log(data);

    if(!this.loginForm.valid)
    {
      window.alert('Not valid');
      return;
    }
  //Poslati podatke serverskoj aplikaciji...
    const sub = this.userServ.login(data.email, data.password).subscribe(e => {
      if (e.user) {
        if (e.user.email !== undefined) {
          this.userServ.storeToken(e.token);

          this.currentEmail = e.user.email;
          this.userServ.putCurrentUser(e.user);
          //this.router.navigate(['/', data.email]);
          this.router.navigate(['/']);
          return;
        }
      }
      else {
        window.alert("Proverite svoje podatke!\nTraženi korisnik ne postoji! ");
        this.loginForm.reset();
        return;
      }
    });
    
    //kraj tog dela
    this.activeSubs.push(sub);


    this.loginForm.reset();
    }

    ngOnDestroy(): void {
      this.activeSubs.forEach((sub: Subscription) => {
        sub.unsubscribe();
      });
    }

}
