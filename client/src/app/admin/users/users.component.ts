import { Component, OnInit } from '@angular/core';

import { Observable } from 'rxjs'; 
import { ActivatedRoute, Router } from '@angular/router';
import { UserService } from '../../services/user.service';
import { UserModel } from '../../models/user.model';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css']
})
export class UsersComponent implements OnInit {
    
  public users: UserModel[];
  
  constructor(
        private route: ActivatedRoute,
        private router: Router,
        private userService: UserService) { 
         this.userService.getAllUsers()
          .subscribe((users: UserModel[]) => {
            this.users = users;
            });        
  }

  ngOnInit(): void {
  }

}
