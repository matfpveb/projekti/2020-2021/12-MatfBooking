import { VillaService } from './../services/villa.service';
import { Router } from '@angular/router';
import { Villa } from './../models/villa.model';
import { FormGroup } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { UserService } from '../services/user.service';
import { UserModel } from '../models/user.model';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-add-villa',
  templateUrl: './add-villa.component.html',
  styleUrls: ['./add-villa.component.css']
})
export class AddVillaComponent implements OnInit {

  newVilla: Villa;
  errors: any[] = [];
  selectedFile: File =null;

  constructor(private httpClient:HttpClient, private villaService: VillaService, private userService: UserService, private router: Router) { }

  ngOnInit(): void {
    this.newVilla = new Villa();
  }

  public currentUser():UserModel{
    return this.userService.currentUser;
  }

  postFile(fileToUpload: File): Observable<any> {
    const endpoint = 'http://localhost:3000/villas/file'
    const formData: FormData = new FormData();
    formData.append('image', fileToUpload, fileToUpload.name);
    return this.httpClient
      .post(endpoint, formData);
  }
  handleImageUpload(files:FileList){
    this.selectedFile=files.item(0);
    this.postFile(this.selectedFile).subscribe((response) => {
      console.log('response received is ', response);
      this.newVilla.images="../../assets/img/gallery/" + this.selectedFile.name;
  })
  }

  public createVilla(){
    this.villaService.addNewVilla(this.newVilla).subscribe(
      (rental: Villa)=>{
        alert('Villa is successfully created!');
        this.router.navigate([`/`])
      },
      (errorresponse: HttpErrorResponse)=>{
          this.errors = errorresponse.error.errors;
      }
    )
  }

}
