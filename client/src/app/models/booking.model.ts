import { HotelModel, Hotel } from "./hotel.model";
import { ApartmentModel, Apartment } from "./apartment.model";
import { VillaModel, Villa } from "./villa.model";
import { UserModel } from "./user.model";

export interface BookingModel {
    _id: string;
    startAt: string;
    endAt: string,
    totalPrice: number,
    guests: number;
    days: number;
    createdAt: string;
    user: UserModel;
    rental: Villa | Hotel | Apartment;
}

export class Booking{

    static readonly DATE_FORMAT = 'Y/MM/DD';

    _id: string;
    startAt: string;
    endAt: string;
    totalPrice: number;
    guests: number;
    days: number;
    createdAt: string;
    user: UserModel;
    rental: Villa | Hotel | Apartment
}
