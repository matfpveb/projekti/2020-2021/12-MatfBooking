export interface LocationModel {
    _id: string;
    width: number;
    height: number
    adress: string;    
}
