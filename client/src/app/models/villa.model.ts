import { LocationModel } from "./location.model";
import { UserModel } from "./user.model";
import { BookingModel, Booking } from './booking.model';

export interface VillaModel {
    _id: string;
    villaName: string;
    type: string;
    numberOfRooms: number; 
    numOfGuests: number;
    location: LocationModel;
    destination: string;
    rentingDate: string;
    dateAvailable: string;
    user: string;
    comments: string[];
    images: string;
    pricePerNight: string;
    checkIn: string;
    checkOut: string;
    status: string;
    amenities: string[];
    reservations: Booking[];
}

export class Villa {
    _id: string;
    user: string | UserModel;
    villaName: string;
    type: string;
    numberOfRooms: number; 
    numOfGuests: number;
    location:string | Location;
    destination: string;
    pricePerNight: string;
    images: string;
    reservations: Booking[];
}

