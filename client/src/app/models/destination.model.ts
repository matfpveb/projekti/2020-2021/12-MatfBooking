import { HotelModel } from "./hotel.model";
import { ApartmentModel } from "./apartment.model";
import { VillaModel } from "./villa.model";

export interface DestinationModel {
    _id: string;
    name: string;
    type: string;
    hotels: string[] | HotelModel[];
    apartments: string[] | ApartmentModel[];
    villas: string[] | VillaModel[];
    description: string;
}
