import { LocationModel } from "./location.model";
import { UserModel } from './user.model';
import { BookingModel, Booking } from './booking.model';

export interface ApartmentModel {
    _id: string;
    apartmentName: string;
    numberOfRooms: number; 
    numOfGuests: number;
    location: LocationModel;
    destination: string;
    rentingDate: string;
    dateAvailable: string;
    user: string;
    comments: string[];
    images: string;
    pricePerNight: string;
    checkIn: string;
    checkOut: string;
    status: string;
    amenities: string[];
    reservations: Booking[];
}


export class Apartment {
    _id: string;
    user: string | UserModel;
    apartmentName: string;
    numberOfRooms: number; 
    numOfGuests: number;
    location:string | LocationModel;
    destination: string;
    pricePerNight: string;
    images: string;
    reservations: Booking[];
}
