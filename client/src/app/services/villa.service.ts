import { UserService } from './user.service';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import {Villa} from '../models/villa.model';
import { HttpClient} from '@angular/common/http';



@Injectable({
  providedIn: 'root'
})
export class VillaService {

  private readonly villasUrl = 'http://localhost:3000/villas/';
  private authHeaderObject:()=>object = ()=>{
    return {headers:{Authorization:"Bearer "+ localStorage.getItem('jwt')}};
  
   }

  constructor(private http: HttpClient) { }
  
  public getVillasById(id: string): Observable<Villa> {
    return this.http.get<Villa>(this.villasUrl + id);
  }
    
  public getVillas(): Observable<any>{
    return this.http.get(this.villasUrl);
   }

   public addNewVilla(data: object): Observable<Villa>{
    return this.http.post<Villa>(this.villasUrl,data, this.authHeaderObject());
  }   

  

}
