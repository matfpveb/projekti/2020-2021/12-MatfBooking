import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';

import { HotelModel, Hotel } from '../models/hotel.model';
import { VillaModel, Villa } from '../models/villa.model';

import { BookingModel, Booking } from '../models/booking.model';
import { LocationModel } from '../models/location.model';

@Injectable({
  providedIn: 'root'
})
export class BookingService {
   private villa: VillaModel;
   private readonly bookingUrl = 'http://localhost:3000/bookings/';
    private readonly villaUrl = 'http://localhost:3000/villa/';

private authHeaderObject:()=>object = ()=>{
    return {headers:{Authorization:"Bearer "+ localStorage.getItem('jwt')}};
  
   }


  constructor(private http: HttpClient, router: Router) {
  }
  
   public getBookings(): Observable<BookingModel[]> {
    return this.http.get<BookingModel[]>(this.bookingUrl);
  }

  public getBookingById(id: string): Observable<BookingModel> {
    return this.http
      .get<BookingModel>(this.bookingUrl + id);
  }
  
  public createBookingVilla(body: any) {
    return this.http.post<any>(this.bookingUrl + 'villa', body, this.authHeaderObject());
   } 
   
   
  public createBookingHotel(body: any) {
    return this.http.post<any>(this.bookingUrl + 'hotel', body, this.authHeaderObject());
   } 
   
  public createBookingApartment(body: any) {
    return this.http.post<any>(this.bookingUrl + 'apartment', body, this.authHeaderObject());
   } 
}
