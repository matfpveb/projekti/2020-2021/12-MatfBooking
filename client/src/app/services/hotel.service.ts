import { UserService } from './user.service';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import {Hotel} from '../models/hotel.model';
import { HttpClient} from '@angular/common/http';



@Injectable({
  providedIn: 'root'
})
export class HotelService {

  private readonly hotelsUrl = 'http://localhost:3000/hotels/';
  private authHeaderObject:()=>object = ()=>{
    return {headers:{Authorization:"Bearer "+ localStorage.getItem('jwt')}};
  
   }

  constructor(private http: HttpClient) { }
  
  public getHotelsById(id: string): Observable<Hotel> {
    return this.http.get<Hotel>(this.hotelsUrl + id);
  }
    
  public getHotels(): Observable<any>{
    return this.http.get(this.hotelsUrl);
   }

   public addNewHotel(data: object): Observable<Hotel>{
    return this.http.post<Hotel>(this.hotelsUrl,data, this.authHeaderObject());
  }   

  

}
