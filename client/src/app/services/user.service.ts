import { Injectable } from '@angular/core';
import { UserModel } from '../models/user.model';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import {first, take} from "rxjs/operators";



/*import {JwtHelperService } from '@auth0/angular-jwt';*//*TODO*/


@Injectable({
  providedIn: 'root'
})
export class UserService  {
  private readonly userUrl ='http://localhost:3000/users/';
  private readonly registerNewUserUrl = "http://localhost:3000/users";

  private allUsers: UserModel[];

  public emails: Array<string> = [];
  public currentUser: UserModel;
  private authHeaderObject:()=>object = ()=>{
  return {headers:{Authorization:"Bearer "+ localStorage.getItem('jwt')}};

 }
/*private helper: JwtHelperService;*/

  constructor(private http: HttpClient) {

    // http zahtev za dohvatanje user-a
    this.http.get<UserModel[]>(this.userUrl)
      .subscribe(res => {
        this.allUsers = res;
      });
      
      this.http.get<UserModel>(this.userUrl+"currentUser",this.authHeaderObject())
      .subscribe(user => {
      	if(user)
      	{
      		this.currentUser=user;
      	}
      });

  
   }

   public getUsers(): UserModel[] {
    return this.allUsers;
  }

  public getAllUsers(): Observable<UserModel[]>{
    return this.http.get<UserModel[]>(this.userUrl);
  }
  
  public getEmails(): Array<string> {
    this.allUsers.forEach(element => {
      this.emails.push(element.email.toString());
    });
    return this.emails;
  }

  public getUserById(): Observable<UserModel> {
    return this.http.get<UserModel>(this.userUrl + 'this.currentUser._id' , this.authHeaderObject());
  }

  public getUserByUsername(username: String): Observable<UserModel> {
    return this.http.get<UserModel>(this.userUrl + "filterByUsername?username=" + username);
  }


  public changeUserInfoData(username: string, email: string){
    const body = { username, email };
    return this.http.patch<any>(this.userUrl + 'change-password/' +  this.currentUser._id, body, this.authHeaderObject());
  }
  
  public registerNewUser(data:object):Observable<any>{
  	return this.http.post<any>(this.registerNewUserUrl,data,this.authHeaderObject());
  }

 /* public updateUser(data:object):Observable<any>{
  	return this.http.put<any>(this.updateUserUrl,data,this.authService.authHeaderObject());
  
  }*/

  public putCurrentUser(user: UserModel) {
    this.currentUser = user;
  }

  public getCurrentUser(): UserModel {
    return this.currentUser;
  }

  public removeCurrentUser():void {
  	this.currentUser=null;
  }


  public login(email: string, password: string) {
    const body = { email, password };

    return this.http.post<any>(this.userUrl + 'login', body);
  }
  
/*TODO spearate this*/
  public storeToken(jwt: string)
  {
    localStorage.jwt = jwt;
   /* localStorage.id = this.helper.decodeToken(jwt).id;*/
  }

    public destroyToken():void{
  	//localStorage.jwt=undefined;
  	localStorage.removeItem('jwt');
  }
  

}
