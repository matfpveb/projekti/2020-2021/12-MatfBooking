const mongoose = require('mongoose');
const Hotels = require('../models/hotels');
const Users = require('../models/user');
const Location= require('../models/locations');
const Destinations = require('../models/destinations');

module.exports.getHotels = async (req, res, next) => {
  try {
    const hotels = await Hotels.find({}).exec();
    res.status(200).json(hotels);
  } catch (err) {
    next(err);
  }
};

module.exports.getUsersHotels = async function (req, res, next) {

    let username=req.params.username;
    
    Users.find({username: username}, function(err, result)
    {
        if (err)
        {
            return res.status(500).json({reason: err});
        }
        if(result.length === 0) {
            return res.status(404).json({messages: "Not exists user with this username!"});
        }
        else{
 
 
           Hotels.find({user: result[0]._id}, {hotelName: 1},function(err, result)
            {
             if (err)
            {
                return res.status(500).json({reason: err});
            }
            
            res.status(200).json({rezultat: result});
    });
    }
});
};
module.exports.getHotelsById = async (req, res, next) => {
  const id = req.params.id;

  try {
    if (id<0) {
      const error = new Error('Not existing id');
      error.status = 400;
      throw error;
    }

    const hotel = await Hotels.findById(id).populate('location').exec();
    if (hotel == null) {
      res.status(404).json();
    } else {
      res.status(200).json(hotel);
    }
  } catch (error) {
    next(error);
  }
};

module.exports.addNewHotel = async function (req, res, next) {

  const adress = req.body.location;

     try {
       if (adress == undefined) {
         const error = new Error('Nedostaje lokacija');
         error.status = 400;
         throw error;
       }
   
       const locationObj = await Location.find({adress: adress}).exec();
       if (locationObj == null) {
         res.status(404).json();
       } else {

    const hotelObject = {
      _id: new mongoose.Types.ObjectId(),
      hotelName: req.body.hotelName,
      numberOfStars: req.body.numberOfStars,
      numberOfRooms: req.body.numberOfRooms,
      numOfGuests: req.body.numOfGuests,
      location: locationObj.map(l=>l._id),
      destination: req.body.destination,
      user: req.user._id,
      images: req.body.images,
      pricePerNight: req.body.pricePerNight
     // reservations: req.body.reservations
      };
    
    const hotel = new Hotels(hotelObject);
    
    let name=nameDest(locationObj[0].adress);
    //console.log(name);
    
    try {
    Destinations.updateOne({name:name}, {$push: {hotels: hotel._id}}, function(){})
        
      Users.updateOne({_id: hotelObject.user}, {$push: {rentals: hotel._id}}, function(){})
       
      const savedHotel = await hotel.save();
      res.status(201).json({
        message: 'Hotel is successfully created',
        hotel: savedHotel,
      });
    } catch (err) {
      next(err);
    }
  }
    } catch (err) {
      next(err);
    }
};


module.exports.addFile = async (req, res, next) => {
  const file = req.file;
  console.log(file.filename);        
  res.send(file);
  };

function nameDest(adress){
    switch(adress){
        case "11000 Belgrade": name = "Belgrade"; break;
        case "31310 Zlatibor": name = "Zlatibor"; break;
        case "81109 Petra": name = "Petra"; break;
        case "36210 Vrnjačka Banja": name = "Vrnjacka_banja"; break;
        case "36354 Kopaonik": name = "Kopaonik"; break;
        case "30100 Venice": name = "Venice"; break;
        case "105-0011 Tokyo": name="Tokyo"; break;
        case "08016 Barcelona": name = "Barcelona"; break;
        case "11311 Egypt": name = "Egypt"; break;
        case "46958 Mexico": name = "Mexico_city"; break;
        case "51133 Abu Dhabi": name = "Abu_dhabi"; break;
        case "22407 Fruška gora": name="Fruska_gora"; break;
        case "11300 Havana": name = "Havana"; break;
        case "08681 Macchu Pichu": name = "Machu_picchu"; break;
        case "18230 Sokobanja": name = "Soko_banja"; break;
        case "21000 Novi Sad": name="Novi_sad"; break;
        case "34300 Arandjelovac": name = "Arandjelovac"; break;
        case "31250 Tara": name="Tara"; break;
        default: "11000 Belgrade"
    }
    return name;
}  
