const mongoose = require('mongoose');
const Location = require('../models/locations');

module.exports.getLocations = async (req, res, next) => {
  try {
    const locations = await Location.find({}).exec();
    res.status(200).json(locations);
  } catch (err) {
    next(err);
  }
};

module.exports.getLocationById = async (req, res, next) => {
  const id = req.params.id;

  try {
    if (id == undefined) {
      const error = new Error('Nedostaje id!');
      error.status = 400;
      throw error;
    }

    const location = await Location.find({_id: id}).exec();
    if (location == null) {
      res.status(404).json();
    } else {
      res.status(200).json(location);
    }
  } catch (error) {
    next(error);
  }
};

    
