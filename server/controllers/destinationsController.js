const mongoose = require('mongoose');
const Destinations = require('../models/destinations');

//izlistati sve informacije o destinacijama 
module.exports.getDestinations = async (req, res, next) => {
    try {
    const destinations = await Destinations.find({}).exec();
    res.status(200).json(destinations);
  } catch (err) {
    next(err);
  }
};

module.exports.getDestinationByType = async (req, res, next) => {
  const type = req.params.type;

  try {
    if (type == undefined) {
      const error = new Error('Nedostaje tip ime!');
      error.status = 400;
      throw error;
    }

    const dest = await Destinations.find({username: username}).exec();
    if (dest == null) {
      res.status(404).json();
    } else {
      res.status(200).json(dest);
    }
  } catch (error) {
    next(error);
  }
};

module.exports.getDestinationById = async function (req, res, next) {
  const destinationId = req.params.destinationId;

  try {
    const destination = await Destinations.findById(destinationId).populate('hotels').populate('villas').populate('apartments').exec();

    // Ukoliko nije pronadjen objekat, prijavi 404
    if (!destination) {
      return res
        .status(404)
        .json({ message: 'The destination with given id does not exist' });
    }

    // Inace, vrati pronadjeni objekat
    res.status(200).json(destination);
  } catch (err) {
    next(err);
  }
};

