const mongoose =require('mongoose');
const Villa = require('../models/villas');
const Booking = require('../models/bookings')

const userSchema = mongoose.Schema({
    _id: mongoose.Schema.Types.ObjectId,
    username: {
        type: String,
        required: true, 
        default: '', 
        trim: true,
        unique: true
    },
    password: {
        type: String,
        required: true,
        default: '', 
        trim: true
    },
    email: {
        type: String,
        default: '', 
        trim: true
    },
    name: {
        type: String,
        required: true, 
        default: '', 
        trim: true
    },
    surname: {
        type: String,
        required: true, 
        default: '', 
        trim: true
    },
    gender: {
        type: String,
        required: true, 
        default: '', 
        trim: true
    },
    userRole: {
        type: String, 
       /* default: 'guest',*/ 
        trim: true,
    },
   rentals: [{
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Villa' | 'Apartment' | 'Hotel',
        default: []
        }],
   bookings: [{
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Booking',
        default: []
        }]

    
});

const User =mongoose.model('User',userSchema);

module.exports=User;


