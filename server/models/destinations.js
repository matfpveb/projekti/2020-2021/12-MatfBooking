const mongoose =require('mongoose');
const Hotel = require("../models/hotels");
const Villa = require("../models/villas");
const Apartment = require("../models/apartments");

const destinationsSchema = mongoose.Schema({
    _id: mongoose.Schema.Types.ObjectId,
    type: {
        type: String,
        required: true
    },
    name:{
        type: String,
        required: true
    },  
    hotels:[{  
        type:mongoose.Schema.Types.ObjectId,
        ref:'Hotel'
    }],
    villas: [{  
        type:mongoose.Schema.Types.ObjectId,
        ref:'Villa'
    }],
    apartments: [{  
        type:mongoose.Schema.Types.ObjectId,
        ref:'Apartment'
    }],
    description: String

});

const Destination =mongoose.model('Destination', destinationsSchema);

module.exports=Destination;
