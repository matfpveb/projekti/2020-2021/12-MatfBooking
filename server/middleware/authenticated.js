const passport=require('passport')
const strategy=require('../strategies/strategyJwt')
const authenticated=passport.authenticate('jwt',{session:false});

module.exports = authenticated;
