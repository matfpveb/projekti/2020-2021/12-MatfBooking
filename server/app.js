const express = require('express');
const passport = require('passport');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');
const cors=require('cors');

const app=express();

const databaseString= 'mongodb://127.0.0.1:27017/booking';

app.use(passport.initialize());
app.use(cors({origin:true,credentials:true}));

mongoose.connect(databaseString, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useCreateIndex: true
});

mongoose.connection.once('open', function(){
    console.log('Uspesno povezivanje!');
});

mongoose.connection.on('error', (error)=>{
    console.log('Error: ', error);
});


app.use(express.json());
app.use(express.urlencoded({
  extended: true
}));

// Implementacija CORS zastite
app.use(function (req, res, next) {
  res.header('Access-Control-Allow-Origin', '*');
  res.header('Access-Control-Allow-Headers', "Origin, X-Requested-With, Content-Type, Accept");

  if (req.method === 'OPTIONS') {
   res.header(
      'Access-Control-Allow-Methods',
      'OPTIONS, GET, POST, PATCH, DELETE'
    );

    return res.status(200).json({});
  }

  next();
});

const userRoutes=require('./routes/api/users');
app.use('/users', userRoutes);

const locationRoutes=require('./routes/api/locations');
app.use('/locations', locationRoutes);

const addressesRoutes=require('./routes/api/addresses');
app.use('/addresses', addressesRoutes);

const apartmentsRoutes=require('./routes/api/apartments');
app.use('/apartments', apartmentsRoutes);

const hotelsRoutes=require('./routes/api/hotels');
app.use('/hotels', hotelsRoutes);

const villasRoutes=require('./routes/api/villas');
app.use('/villas', villasRoutes);

const bookingsRoutes=require('./routes/api/bookings');
app.use('/bookings', bookingsRoutes);

const destinationsRoutes=require('./routes/api/destinations');
app.use('/destinations', destinationsRoutes);

const indexAPIRoutes = require('./routes/api/indexAPI');
app.use('/', indexAPIRoutes);

app.use(function(req, res, next){
    const error = new Error('Zahtev nije podrzan od servera!');
    error.status = 405;

    next(error);
});

app.use(function(error,req, res, next){
    res.status(error.status || 500).json({
        error: {
            message: error.message,
            status: error.status || 500,
            stack: error.stack    
        },
    });    
});

module.exports = app;
