const express=require('express');
const router=express.Router();

const controller = require('../../controllers/destinationsController');

//http:://localhost:3000/api/destinations
router.get('/', controller.getDestinations);

//http:://localhost:3000/api/destinations/id
router.get('/:destinationId', controller.getDestinationById);

module.exports = router;

