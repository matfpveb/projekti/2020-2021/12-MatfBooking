const express=require('express');
const router=express.Router();
const authenticated=require('../../middleware/authenticated');
const multer = require('multer');


const controller = require('../../controllers/apartmentsController');

//define storage for the images
const storage = multer.diskStorage({
    //destination for files
    destination: function(request, file, callback){
        callback(null,'./../client/src/assets/img/gallery');
    },

    //addd back the extension
    filename: function(request, file, callback){
        callback(null,file.originalname);
    }
});

//upload parameters from multer
const upload = multer({
    storage:storage,
    limits:{
        fieldSize:1024*1024*3
    },
});

//http:://localhost:3000/api/apartments
router.get('/', controller.getApartments);

//http:://localhost:3000/api/apartments/username
router.get('/user/:username', controller.getUsersApartment);

router.get('/:id',controller.getApartmentById);

router.post('/',authenticated, controller.addNewApartment);

router.post('/file', upload.single('image'), controller.addFile);

module.exports = router;

