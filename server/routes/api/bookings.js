const express=require('express');
const router=express.Router();
const authenticated=require('../../middleware/authenticated');


const controller = require('../../controllers/bookingsController');

//http:://localhost:3000/api/bookings
router.get('/', controller.getBookings);

router.get('/:id', controller.getBookingById);

router.post('/villa', authenticated, controller.createBookingVilla);

router.post('/hotel', authenticated, controller.createBookingHotel);

router.post('/apartment', authenticated, controller.createBookingApartment);


module.exports = router;

