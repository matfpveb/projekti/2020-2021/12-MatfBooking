# Project MatfBooking

Osnovna ideja: Osmišljavanje aplikacije za rezervaciju apartmana, hotela i vila (booking sajt), sa mogućim dodacima i izmenama. 

Koristili smo MongoDB bazu podataka u kojoj smo čuvali informacije i podatke vezane za lokacije, adrese, korisnike, destinacije, vile, hotele, apartmane...
Aplikacija koristi 3 grupe: gost, domaćin i administrator.
Omogućena je prijava korisnika, kao i registracija novih, dodvanje novog smeštaja u bazu podataka, pregled rezervacija, rezervisanje smeštaja, prikaz lokacije na mapi..
Aplikacija sadrži dve vrste destinacija (domaće i strane), i u svakoj od njih korisnik moze pronaći smeštaj, u zavisnosti od svojih interesa, i rezervisati ga za datum koji mu odgovara i koji je, naravno, slobodan.


## Tehnologije :books: :

- Angular (client side)

- Node.js/Express.js (server side)
- MongoDB

## Pokretanje aplikacije :wrench: :

- `git clone https://gitlab.com/matfpveb/projekti/2020-2021/12-MatfBooking.git`

**Inicijalizacija baze:** :hammer: :

U terminalu napraviti praznu bazu booking:
- `mongo`
- `show dbs`
- `use booking`

Zatim se pozicionirati i izvršiti skript:
- `cd 12-MatfBooking/scripts/base.sh`
- `./base.sh`


**Server**
- `cd 12-MatfBooking/server`
- `npm install`
- `npm start`

**Klijent**


- `cd 12-MatfBooking/client`
- `npm install`
- `ng serve --live-reload false`


## Izgled baze:
![](client/src/assets/img/baza.png)


## Developers

- [Jovana Markovic, 144/2017](https://gitlab.com/jovanaMarkovic) :woman:
- [Sara Mihailovic, 293/2017](https://gitlab.com/SaraMihailovic) :woman:
- [Tamara Stojkovic, 178/2017](https://gitlab.com/tamarastojkovic) :woman:
- [Tamara Sljivic, 107/2017](https://gitlab.com/TamaraSljivic) :woman:
